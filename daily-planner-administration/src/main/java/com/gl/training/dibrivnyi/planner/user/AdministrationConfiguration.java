package com.gl.training.dibrivnyi.planner.user;

import com.gl.training.dibrivnyi.planner.common.PersistenceConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:03
 */
@Configuration
@Import({PersistenceConfiguration.class, MailConfiguration.class})
@EnableJpaRepositories(basePackageClasses = AdministrationConfiguration.class)
@ComponentScan(basePackageClasses = AdministrationConfiguration.class)
public class AdministrationConfiguration {
}
