package com.gl.training.dibrivnyi.planner.user.service.api;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:42
 */
public interface MailService {

    void sendEmail(List<String> emails, String subject, String description);
}
