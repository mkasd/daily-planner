package com.gl.training.dibrivnyi.planner.user.repository;

import com.gl.training.dibrivnyi.planner.user.model.Permission;
import com.gl.training.dibrivnyi.planner.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:22
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Method for loading user details by login
     *
     * @param login - user login
     * @return - loaded user details
     */
    @Query("select u from User u where u.login =:login")
    User findOneByLogin(@Param("login") String login);

    /**
     * Method for loading all logins
     *
     * @return - list of logins
     */
    @Query("select u.login from User u")
    List<String> findAllLogins();

    /**
     * Method for loading all emails
     *
     * @return - list of emails
     */
    @Query("select u.email from User u")
    List<String> findAllEmails();
}
