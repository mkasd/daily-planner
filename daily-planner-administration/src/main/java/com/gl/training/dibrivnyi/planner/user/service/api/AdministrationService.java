package com.gl.training.dibrivnyi.planner.user.service.api;

import com.gl.training.dibrivnyi.planner.user.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.user.model.Permission;
import com.gl.training.dibrivnyi.planner.user.model.User;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:25
 */
public interface AdministrationService {

    void addUser(User user);

    void removeUser(Long id);

    List<User> listAllUsers();

    User getUser(String login) throws UserNotFoundException;

    List<Permission> findUserPermissionByLogin(String login);

    void updateUser(User user);

    List<String> findAllLogins();

    List<String> findAllEmails();
}
