package com.gl.training.dibrivnyi.planner.user;

import com.gl.training.dibrivnyi.planner.common.ExternalConfiguration;
import com.gl.training.dibrivnyi.planner.common.properties.MailSenderProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:18
 */
@Configuration
@Import({ExternalConfiguration.class})
public class MailConfiguration {

    @Autowired
    private MailSenderProperties mailSenderProperties;

    @Autowired
    private Properties javaMailProperties;

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setPassword(mailSenderProperties.getPassword());
        mailSender.setUsername(mailSenderProperties.getUsername());
        mailSender.setHost(mailSenderProperties.getHost());
        mailSender.setPort(Integer.valueOf(mailSenderProperties.getPort()));
        mailSender.setProtocol(mailSenderProperties.getProtocol());
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }
}
