package com.gl.training.dibrivnyi.planner.user.exception;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         17:03
 *
 *         If user not found in database, should be thrown this exception
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
        super();
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
