package com.gl.training.dibrivnyi.planner.user.service;

import com.gl.training.dibrivnyi.planner.user.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.user.model.Permission;
import com.gl.training.dibrivnyi.planner.user.model.Role;
import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.repository.PermissionRepository;
import com.gl.training.dibrivnyi.planner.user.repository.UserRepository;
import com.gl.training.dibrivnyi.planner.user.service.api.AdministrationService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:27
 */
@Service
public class AdministrationServiceImpl implements AdministrationService {

    private static final Logger LOGGER = Logger.getLogger(AdministrationServiceImpl.class.getSimpleName());
    private UserRepository userRepository;
    private PermissionRepository permissionRepository;

    @Autowired
    public void setPermissionRepository(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Method for persisting users. Encodes input user password to sha1Hex;
     * Sets user default role as 'USER'
     *
     * @param user - user to persist
     */
    @Override
    public void addUser(User user) {
        String encodedPassword = DigestUtils.sha1Hex(user.getPassword());
        user.setPassword(encodedPassword);

        Permission userPermission = new Permission();
        userPermission.setOwner(user);
        userPermission.setRole(Role.USER);

        userRepository.saveAndFlush(user);
        permissionRepository.saveAndFlush(userPermission);
        LOGGER.info(String.format("User <%s> was saved in database", user));
    }

    /**
     * Method for removing user by his unique id
     * If user not found it do nothing
     *
     * @param id - user id that will be deleted
     */
    @Override
    public void removeUser(Long id) {
        User user = userRepository.findOne(id);
        if (user != null) {
            List<Permission> userPermissions = permissionRepository.findUserPermissions(user.getLogin());
            permissionRepository.delete(userPermissions);
            userRepository.delete(user);
            LOGGER.info(String.format("User %s was removed", user));
        }
    }

    /**
     * Method for loading list of registered users
     *
     * @return list of all users
     */
    @Override
    public List<User> listAllUsers() {
        LOGGER.info("Start loading list of all users");
        List<User> users = userRepository.findAll();
        LOGGER.info("Totally loaded users: " + users.size());
        return users;
    }

    /**
     * Method for loading users details
     *
     * @param login - user login, which details will be loaded
     * @return user details
     * @throws UserNotFoundException - if user not found
     */
    @Override
    public User getUser(String login) throws UserNotFoundException {
        User user = userRepository.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException(login);
        }
        LOGGER.info(String.format("User %s was found in database", user));
        return user;
    }

    /**
     * Method for loading user permission by user login
     *
     * @param login - user login, which permission should be found
     * @return - list of user permission
     */
    @Override
    public List<Permission> findUserPermissionByLogin(String login) {
        List<Permission> permissions = permissionRepository.findUserPermissions(login);
        LOGGER.info(String.format("User %s has permissions %s", login, permissions.toString()));
        return permissions;
    }

    /**
     * Method for updating user. If user is new, it will be saved as a new user
     *
     * @param user - user details to update / save
     */
    @Override
    public void updateUser(User user) {
        userRepository.saveAndFlush(user);
        LOGGER.info(String.format("User %s was updated", user));
    }

    /**
     * Method for loading all logins
     *
     * @return - list of all logins
     */
    @Override
    public List<String> findAllLogins() {
        return userRepository.findAllLogins();
    }

    /**
     * Method for loading all email
     *
     * @return - list of all email
     */
    @Override
    public List<String> findAllEmails() {
        return userRepository.findAllEmails();
    }
}
