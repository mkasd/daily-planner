package com.gl.training.dibrivnyi.planner.user.service;

import com.gl.training.dibrivnyi.planner.user.service.api.MailService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:44
 */
@Component
public class MailServiceImpl implements MailService {

    private static final Logger LOGGER = Logger.getLogger(MailServiceImpl.class.getSimpleName());
    private JavaMailSender mailSender;

    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Method for sending emails to list of recipients
     *
     * @param emails - list of all recipients
     * @param subject - message subject
     * @param description - message description
     */
    @Override
    public void sendEmail(List<String> emails, String subject, String description) {
        Validate.notEmpty(emails);
        Validate.notEmpty(subject);
        Validate.notEmpty(description);

        String[] recipients = emails.toArray(new String[emails.size()]);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText(description);
        message.setSubject(subject);
        message.setSentDate(new Date());
        message.setTo(recipients);

        mailSender.send(message);
        LOGGER.info(String.format("Message with subject <%s> was sent to <%s> recipients", subject, emails.size()));
    }
}
