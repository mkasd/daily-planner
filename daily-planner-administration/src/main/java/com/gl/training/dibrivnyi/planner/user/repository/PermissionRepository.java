package com.gl.training.dibrivnyi.planner.user.repository;

import com.gl.training.dibrivnyi.planner.user.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         12.08.14
 *         23:29
 */
@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {

    /**
     * Method for loading user permissions
     *
     * @param login - user login
     * @return - list of available user permissions
     */
    @Query("select p from Permission p where p.owner.login =:login")
    List<Permission> findUserPermissions(@Param("login") String login);
}
