package com.gl.training.dibrivnyi.planner.user.service;

import com.gl.training.dibrivnyi.planner.user.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.user.model.Permission;
import com.gl.training.dibrivnyi.planner.user.model.Role;
import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.repository.PermissionRepository;
import com.gl.training.dibrivnyi.planner.user.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:43
 */
public class AdministrationServiceImplTest {

    @Mock
    private PermissionRepository permissionRepository;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private AdministrationServiceImpl administrationService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(userRepository, permissionRepository);
    }

    @Test
    public void testAddUser() throws Exception {
        User user = getUser("user", "email");
        user.setPassword("asd");
        Permission permission = getUserPermission(Role.USER, "user");
        when(userRepository.saveAndFlush(user)).thenReturn(user);
        when(permissionRepository.saveAndFlush(permission)).thenReturn(permission);
        administrationService.addUser(user);

        verify(userRepository).saveAndFlush(any(User.class));
        verify(permissionRepository).saveAndFlush(any(Permission.class));
    }

    @Test
    public void testListAllUsers() throws Exception {
        User ue1 = getUser("test1", "testEmail1");
        User ue2 = getUser("test2", "testEmail2");

        when(userRepository.findAll()).thenReturn(Arrays.asList(ue1, ue2));

        List<User> users = administrationService.listAllUsers();
        assertThat(users.size()).isEqualTo(2);
        assertThat(users).containsOnly(ue1, ue2);
        verify(userRepository).findAll();
    }

    @Test
    public void testListAllUsers_emptyUserList() throws Exception {
        when(userRepository.findAll()).thenReturn(new ArrayList<User>());

        List<User> users = administrationService.listAllUsers();
        assertThat(users).hasSize(0);
        verify(userRepository).findAll();
    }

    @Test
    public void removeUser_no_such_user() throws Exception {
        when(userRepository.findOne(any(Long.class))).thenReturn(null);

        administrationService.removeUser(1234l);

        verify(userRepository).findOne(1234l);
    }

    @Test
    public void removeUser() throws Exception {
        User user = getUser("user1", "mail1");
        when(userRepository.findOne(any(Long.class))).thenReturn(user);
        when(permissionRepository.findUserPermissions(anyString())).thenReturn(new ArrayList<Permission>());

        administrationService.removeUser(1234l);

        verify(userRepository).findOne(1234l);
        verify(userRepository).delete(user);
        verify(permissionRepository).findUserPermissions("user1");
        verify(permissionRepository).delete(any(List.class));
    }

    @Test
    public void testUpdateUser() throws Exception {
        User user = getUserEntityMock("user1", "mail1");
        when(userRepository.saveAndFlush(any(User.class))).thenReturn(user);

        administrationService.updateUser(user);

        verifyZeroInteractions(user);
        verify(userRepository).saveAndFlush(user);
    }

    @Test
    public void testUpdateUser_null_user() throws Exception {
        administrationService.updateUser(null);

        verify(userRepository).saveAndFlush(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUser_null_login() throws Exception {
        when(userRepository.findOneByLogin(null)).thenReturn(null);

        try {
            administrationService.getUser(null);
        } catch (Exception e) {
            verify(userRepository).findOneByLogin(null);
            throw e;
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUser_empty_login() throws Exception {
        when(userRepository.findOneByLogin(anyString())).thenReturn(null);

        try {
            administrationService.getUser("");
        } catch (Exception e) {
            verify(userRepository).findOneByLogin("");
            throw e;
        }
    }

    @Test
    public void testGetUser() throws Exception {
        User user = getUserEntityMock("login", "email");
        when(userRepository.findOneByLogin(anyString())).thenReturn(user);

        User foundUser = administrationService.getUser("login");

        assertThat(foundUser).isEqualTo(user);

        verify(userRepository).findOneByLogin("login");
        verifyZeroInteractions(user);
    }

    @Test
    public void testFindUserPermissionByLogin_null_login() throws Exception {
        List<Permission> mock = mock(List.class);
        when(permissionRepository.findUserPermissions(null)).thenReturn(mock);

        List<Permission> permissions = administrationService.findUserPermissionByLogin(null);

        assertThat(mock).isEqualTo(permissions);
        assertThat(mock).hasSize(0);
        verify(permissionRepository).findUserPermissions(null);
    }

    @Test
    public void testFindUserPermissionByLogin_empty_login() throws Exception {
        List<Permission> mock = mock(List.class);
        when(permissionRepository.findUserPermissions(anyString())).thenReturn(mock);

        List<Permission> permissions = administrationService.findUserPermissionByLogin("");

        assertThat(mock).isEqualTo(permissions);
        assertThat(mock).hasSize(0);
        verify(permissionRepository).findUserPermissions("");
    }

    @Test
    public void testFindUserPermissionByLogin() throws Exception {
        List<Permission> expectedPermissions = Arrays.asList(getUserPermission(Role.ADMIN, "login"), getUserPermission(Role.USER, "login"));
        when(permissionRepository.findUserPermissions(anyString())).thenReturn(expectedPermissions);

        List<Permission> actualPermission = administrationService.findUserPermissionByLogin("login");

        assertThat(actualPermission).isEqualTo(expectedPermissions);
        assertThat(actualPermission).hasSize(2);
        assertThat(actualPermission).extracting("owner").extracting("login").containsOnly("login");

        verify(permissionRepository).findUserPermissions("login");
    }

    @Test
    public void testFindAllLogins() throws Exception {
        List<String> mock = mock(List.class);
        when(userRepository.findAllLogins()).thenReturn(mock);

        List<String> allLogins = administrationService.findAllLogins();

        assertThat(mock).isEqualTo(allLogins);
        verifyZeroInteractions(mock);
        verify(userRepository).findAllLogins();
    }


    @Test
    public void testFindAllEmail() throws Exception {
        List<String> mock = mock(List.class);
        when(userRepository.findAllEmails()).thenReturn(mock);

        List<String> allEmails = administrationService.findAllEmails();

        assertThat(mock).isEqualTo(allEmails);
        verifyZeroInteractions(mock);
        verify(userRepository).findAllEmails();
    }

    private User getUser(String login, String email) {
        User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        return user;
    }

    private Permission getUserPermission(Role role, String login) {
        Permission permission = new Permission();
        permission.setOwner(getUser(login, "stub"));
        permission.setRole(role);
        return permission;
    }

    private User getUserEntityMock(String login, String email) {
        User user = mock(User.class);
        user.setLogin(login);
        user.setEmail(email);
        verify(user).setLogin(anyString());
        verify(user).setEmail(anyString());
        return user;
    }
}
