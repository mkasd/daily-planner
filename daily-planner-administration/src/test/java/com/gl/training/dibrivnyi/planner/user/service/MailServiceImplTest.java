package com.gl.training.dibrivnyi.planner.user.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junitparams.JUnitParamsRunner.$;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         12:06
 */
@RunWith(JUnitParamsRunner.class)
public class MailServiceImplTest {

    @Mock
    private JavaMailSender mailSender;
    @InjectMocks
    private MailServiceImpl mailService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(mailSender);
    }

    @Test
    public void testSendEmail() throws Exception {
        doNothing().when(mailSender).send(any(SimpleMailMessage.class));

        List<String> emails = new ArrayList<>();
        emails.add("email");

        mailService.sendEmail(emails, "subject", "description");
        
        verify(mailSender).send(any(SimpleMailMessage.class));
    }

    public Object[] parametersForTestSendEmail_null_input() {
        List<String> notEmptyEmailsList = new ArrayList<>();
        notEmptyEmailsList.add("email");
        return $(
                $(null, "subject", "description"),
                $(notEmptyEmailsList, null, "description"),
                $(notEmptyEmailsList, "subject", null)
        );
    }

    @Test(expected = NullPointerException.class)
    @Parameters
    public void testSendEmail_null_input(List<String> emails, String subject, String description) throws Exception {
        mailService.sendEmail(emails, subject, description);
    }

    public Object[] parametersForTestSendEmail_empty_input() {
        List<String> notEmptyEmailsList = new ArrayList<>();
        notEmptyEmailsList.add("email");
        return $(
                $(Collections.<String>emptyList(), "subject", "description"),
                $(notEmptyEmailsList, "", "description"),
                $(notEmptyEmailsList, "subject", "")
        );
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters
    public void testSendEmail_empty_input(List<String> emails, String subject, String description) throws Exception {
        mailService.sendEmail(emails, subject, description);
    }
}
