package com.gl.training.dibrivnyi.planner.ui.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         18:01
 */
public class UISecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
