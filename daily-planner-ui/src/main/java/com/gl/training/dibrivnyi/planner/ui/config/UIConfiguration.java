package com.gl.training.dibrivnyi.planner.ui.config;

import com.gl.training.dibrivnyi.planner.middle.DailyPlannerManagementConfiguration;
import com.gl.training.dibrivnyi.planner.user.AdministrationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Alexandr Dibrivnyi
 *         05.08.14
 *         22:05
 */
@Configuration
@ComponentScan(basePackages = "com.gl.training.dibrivnyi.planner.ui")
@Import({AdministrationConfiguration.class, DailyPlannerManagementConfiguration.class})
public class UIConfiguration {
}
