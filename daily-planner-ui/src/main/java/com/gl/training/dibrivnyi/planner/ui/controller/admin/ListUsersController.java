package com.gl.training.dibrivnyi.planner.ui.controller.admin;

import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.service.api.AdministrationService;
import org.apache.commons.codec.digest.DigestUtils;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         22:25
 */
@Named
@SessionScoped
public class ListUsersController {

    @Autowired
    private AdministrationService administrationService;
    private User selectedUser;
    private boolean viewDetailsDisabled = true;

    public List<User> getAllUsers() {
        return administrationService.listAllUsers();
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public boolean isViewDetailsDisabled() {
        return viewDetailsDisabled;
    }

    public void setViewDetailsDisabled(boolean viewDetailsDisabled) {
        this.viewDetailsDisabled = viewDetailsDisabled;
    }

    public void onRowSelect(SelectEvent event) {
        viewDetailsDisabled = false;
    }

    public void updateSelectedUser() {
        String notEncodedPassword = selectedUser.getPassword();
        selectedUser.setPassword(DigestUtils.sha1Hex(notEncodedPassword));
        administrationService.updateUser(selectedUser);

        FacesMessage msg = new FacesMessage();
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        msg.setSummary(String.format("User <%s> was updated", selectedUser.getLogin()));
        FacesContext.getCurrentInstance().addMessage(null, msg);

        selectedUser = null;
    }
}
