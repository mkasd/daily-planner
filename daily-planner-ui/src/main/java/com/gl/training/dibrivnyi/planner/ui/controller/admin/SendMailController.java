package com.gl.training.dibrivnyi.planner.ui.controller.admin;

import com.gl.training.dibrivnyi.planner.user.service.api.MailService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         12:23
 */
@Named
@ApplicationScoped
public class SendMailController {

    @Autowired
    private MailService mailService;
    private List<String> recipients;
    private String subject;
    private String description;

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void sendMessage() {
        mailService.sendEmail(recipients, subject, description);

        FacesMessage msg = new FacesMessage();
        msg.setSummary(String.format("Message with subject <%s> was sent to <%s> recipients", subject, recipients.size()));
        msg.setSeverity(FacesMessage.SEVERITY_INFO);

        FacesContext.getCurrentInstance().addMessage(null, msg);
        recipients = null;
        subject = null;
        description = null;
    }
}
