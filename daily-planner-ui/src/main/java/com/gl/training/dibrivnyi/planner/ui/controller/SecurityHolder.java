package com.gl.training.dibrivnyi.planner.ui.controller;

import com.gl.training.dibrivnyi.planner.user.model.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;
import java.util.Collection;

/**
 * @author Alexandr Dibrivnyi
 *         19.08.14
 *         0:10
 */
@Named
@ApplicationScoped
public class SecurityHolder {

    public boolean isUserAdmin() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().equals(Role.ADMIN.name())) {
                return true;
            }
        }
        return false;
    }
}
