package com.gl.training.dibrivnyi.planner.ui.controller.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.logging.Logger;

/**
 * @author Alexandr Dibrivnyi
 *         15.08.14
 *         22:12
 */
@Named
@RequestScoped
public class LoginController {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getSimpleName());
    @Autowired
    private AuthenticationManager authenticationManager;
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean performLogin() {
        boolean loginSuccessful;
        try {
            Authentication token = new UsernamePasswordAuthenticationToken(login, password);
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            loginSuccessful = true;
            LOGGER.info("Login successful. Navigating to plans view");
        } catch (AuthenticationException e) {
            String errorMessage = "Login or password is incorrect";
            FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_WARN);
            msg.setSummary(errorMessage);

            FacesContext.getCurrentInstance().addMessage(null, msg);
            loginSuccessful = false;
            LOGGER.warning(errorMessage);
        }
        return loginSuccessful;
    }
}
