package com.gl.training.dibrivnyi.planner.ui.config;

import com.gl.training.dibrivnyi.planner.common.ExternalConfiguration;
import com.gl.training.dibrivnyi.planner.common.properties.SecurityProperties;
import com.gl.training.dibrivnyi.planner.user.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         16:44
 */
@Configuration
@Import({ExternalConfiguration.class})
@EnableWebSecurity
public class UISecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(getShaPasswordEncoder())
                .and()
                .inMemoryAuthentication()
                .withUser(securityProperties.getDefaultAdminLogin())
                .password(securityProperties.getDefaultAdminPassword())
                .roles(Role.ADMIN.name());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/main_index.xhtml", "/login").permitAll()
                .antMatchers("/plans").access("hasRole('ADMIN') or hasRole('USER')")
                .antMatchers("/plans/**").access("hasRole('ADMIN') or hasRole('USER')")
                .antMatchers("/admin/**").access("hasRole('ADMIN')");

        http.logout()
                .logoutSuccessUrl("/")
                .logoutUrl("/logout")
                .invalidateHttpSession(true);
    }

    @Bean
    public ShaPasswordEncoder getShaPasswordEncoder() {
        return new ShaPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}
