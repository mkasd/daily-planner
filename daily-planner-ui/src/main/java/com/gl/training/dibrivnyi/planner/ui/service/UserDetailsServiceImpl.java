package com.gl.training.dibrivnyi.planner.ui.service;

import com.gl.training.dibrivnyi.planner.user.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.user.model.Permission;
import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.service.api.AdministrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         16:54
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private AdministrationService administrationService;

    @Autowired
    public void setAdministrationService(AdministrationService administrationService) {
        this.administrationService = administrationService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            User user = administrationService.getUser(login);
            List<Permission> permissions = administrationService.findUserPermissionByLogin(login);
            Set<GrantedAuthority> roles = new HashSet<>();
            for (Permission permission : permissions) {
                roles.add(new SimpleGrantedAuthority(permission.getRole().name()));
            }
            return new org.springframework.security.core.userdetails.User(user.getLogin(),
                    user.getPassword(),
                    roles);
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
