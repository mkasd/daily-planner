package com.gl.training.dibrivnyi.planner.ui.controller.admin;

import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.service.api.AdministrationService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         20:36
 */
@Named
@SessionScoped
public class AddUserController {

    @Autowired
    private AdministrationService administrationService;

    private User user;

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void saveUser() {
        administrationService.addUser(user);
        user = null;

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful", "User successfully added"));
    }
}
