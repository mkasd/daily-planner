CREATE TABLE permission
(
  id bigint NOT NULL,
  role character varying(255),
  login character varying(64),
  CONSTRAINT permission_pkey PRIMARY KEY (id),
  CONSTRAINT fk_permission_table FOREIGN KEY (login)
      REFERENCES users (login) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)