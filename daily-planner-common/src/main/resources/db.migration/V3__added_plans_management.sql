CREATE TABLE daily_plan
(
  id bigint NOT NULL,
  date date NOT NULL,
  state character varying(255) NOT NULL,
  owner bigint NOT NULL,
  CONSTRAINT daily_plan_pkey PRIMARY KEY (id),
  CONSTRAINT fk_daily_plan FOREIGN KEY (owner)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE task
(
  id bigint NOT NULL,
  description character varying(40096) NOT NULL,
  end_time time without time zone NOT NULL,
  start_time time without time zone NOT NULL,
  title character varying(255) NOT NULL,
  task bigint,
  CONSTRAINT task_pkey PRIMARY KEY (id),
  CONSTRAINT fk_task FOREIGN KEY (task)
      REFERENCES daily_plan (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)