package com.gl.training.dibrivnyi.planner.common.properties.builder;

import com.gl.training.dibrivnyi.planner.common.properties.JdbcProperties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:41
 */
public class JdbcPropertiesBuilder {
    private String jndiName;

    private JdbcPropertiesBuilder() {
    }

    public static JdbcPropertiesBuilder aJdbcProperties() {
        return new JdbcPropertiesBuilder();
    }

    public JdbcPropertiesBuilder withJndiName(String jndiName) {
        this.jndiName = jndiName;
        return this;
    }

    public JdbcProperties build() {
        JdbcProperties jdbcProperties = new JdbcProperties();
        jdbcProperties.setJndiName(jndiName);
        return jdbcProperties;
    }
}
