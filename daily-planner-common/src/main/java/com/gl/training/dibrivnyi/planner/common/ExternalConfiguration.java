package com.gl.training.dibrivnyi.planner.common;

import com.gl.training.dibrivnyi.planner.common.properties.HibernateProperties;
import com.gl.training.dibrivnyi.planner.common.properties.JdbcProperties;
import com.gl.training.dibrivnyi.planner.common.properties.MailSenderProperties;
import com.gl.training.dibrivnyi.planner.common.properties.SecurityProperties;
import com.gl.training.dibrivnyi.planner.common.properties.builder.HibernatePropertiesBuilder;
import com.gl.training.dibrivnyi.planner.common.properties.builder.JdbcPropertiesBuilder;
import com.gl.training.dibrivnyi.planner.common.properties.builder.MailSenderPropertiesBuilder;
import com.gl.training.dibrivnyi.planner.common.properties.builder.SecurityPropertiesBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:36
 */
@Configuration
public class ExternalConfiguration {

    private static final String CONFIG_NAME = "planner.properties";
    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    private static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.quitwait";
    private static final String MAIL_SMTP_QUITWAIT = "mail.smtp.starttls.enable";
    private Properties externalConfig;

    public ExternalConfiguration() {
        externalConfig = new Properties();
        try {
            externalConfig.load(this.getClass().getClassLoader().getResourceAsStream(CONFIG_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public JdbcProperties jdbcProperties() {
        JdbcPropertiesBuilder builder = JdbcPropertiesBuilder.aJdbcProperties();
        return builder.withJndiName(externalConfig.getProperty("jdbc.jndi.name")).build();
    }

    @Bean
    public HibernateProperties hibernateProperties() {
        HibernatePropertiesBuilder builder = HibernatePropertiesBuilder.aHibernateProperties();
        return builder.withHibernateDialect(externalConfig.getProperty("hibernate.dialect"))
                .withHibernateHbm2Ddl(externalConfig.getProperty("hibernate.hbm2ddl.auto"))
                .withHibernateShowSql(externalConfig.getProperty("hibernate.show_sql")).build();
    }

    @Bean
    public MailSenderProperties mailSenderProperties() {
        MailSenderPropertiesBuilder builder = MailSenderPropertiesBuilder.aMailSenderProperties();
        return builder
                .withHost(externalConfig.getProperty("mail.host"))
                .withPort(externalConfig.getProperty("mail.port"))
                .withProtocol(externalConfig.getProperty("mail.protocol"))
                .withPassword(externalConfig.getProperty("mail.password"))
                .withUsername(externalConfig.getProperty("mail.username")).build();
    }

    @Bean
    public Properties javaMailProperties() {
        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty(MAIL_SMTP_AUTH, externalConfig.getProperty(MAIL_SMTP_AUTH));
        javaMailProperties.setProperty(MAIL_SMTP_STARTTLS_ENABLE, externalConfig.getProperty(MAIL_SMTP_STARTTLS_ENABLE));
        javaMailProperties.setProperty(MAIL_SMTP_QUITWAIT, externalConfig.getProperty(MAIL_SMTP_QUITWAIT));
        return javaMailProperties;
    }

    @Bean
    public SecurityProperties securityProperties() {
        SecurityPropertiesBuilder builder = SecurityPropertiesBuilder.aSecurityProperties();
        return builder
                .withDefaultAdminLogin(externalConfig.getProperty("default.admin.login"))
                .withDefaultAdminPassword(externalConfig.getProperty("default.admin.password"))
                .build();
    }
}
