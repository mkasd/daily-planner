package com.gl.training.dibrivnyi.planner.common.properties.builder;

import com.gl.training.dibrivnyi.planner.common.properties.HibernateProperties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:56
 */
public class HibernatePropertiesBuilder {
    private String hibernateDialect;
    private String hibernateShowSql;
    private String hibernateHbm2Ddl;

    private HibernatePropertiesBuilder() {
    }

    public static HibernatePropertiesBuilder aHibernateProperties() {
        return new HibernatePropertiesBuilder();
    }

    public HibernatePropertiesBuilder withHibernateDialect(String hibernateDialect) {
        this.hibernateDialect = hibernateDialect;
        return this;
    }

    public HibernatePropertiesBuilder withHibernateShowSql(String hibernateShowSql) {
        this.hibernateShowSql = hibernateShowSql;
        return this;
    }

    public HibernatePropertiesBuilder withHibernateHbm2Ddl(String hibernateHbm2Ddl) {
        this.hibernateHbm2Ddl = hibernateHbm2Ddl;
        return this;
    }

    public HibernateProperties build() {
        HibernateProperties hibernateProperties = new HibernateProperties();
        hibernateProperties.setHibernateDialect(hibernateDialect);
        hibernateProperties.setHibernateShowSql(hibernateShowSql);
        hibernateProperties.setHibernateHbm2Ddl(hibernateHbm2Ddl);
        return hibernateProperties;
    }
}
