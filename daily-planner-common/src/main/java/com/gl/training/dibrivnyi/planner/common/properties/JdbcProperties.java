package com.gl.training.dibrivnyi.planner.common.properties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:37
 */
public class JdbcProperties {

    private String jndiName;

    public String getJndiName() {
        return jndiName;
    }

    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }
}
