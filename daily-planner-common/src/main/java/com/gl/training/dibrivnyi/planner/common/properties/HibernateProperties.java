package com.gl.training.dibrivnyi.planner.common.properties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:47
 */
public class HibernateProperties {

    private String hibernateDialect;
    private String hibernateShowSql;
    private String hibernateHbm2Ddl;

    public String getHibernateHbm2Ddl() {
        return hibernateHbm2Ddl;
    }

    public void setHibernateHbm2Ddl(String hibernateHbm2Ddl) {
        this.hibernateHbm2Ddl = hibernateHbm2Ddl;
    }

    public String getHibernateDialect() {
        return hibernateDialect;
    }

    public void setHibernateDialect(String hibernateDialect) {
        this.hibernateDialect = hibernateDialect;
    }

    public String getHibernateShowSql() {
        return hibernateShowSql;
    }

    public void setHibernateShowSql(String hibernateShowSql) {
        this.hibernateShowSql = hibernateShowSql;
    }
}
