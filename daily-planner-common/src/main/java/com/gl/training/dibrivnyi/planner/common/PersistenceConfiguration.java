package com.gl.training.dibrivnyi.planner.common;

import com.gl.training.dibrivnyi.planner.common.properties.HibernateProperties;
import com.gl.training.dibrivnyi.planner.common.properties.JdbcProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:12
 */
@Configuration
@Import(ExternalConfiguration.class)
@EnableJpaRepositories(basePackages = "com.gl.training.dibrivnyi.planner")
public class PersistenceConfiguration {

    private static final String HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String HIBERNATE_HBM_2_DLL = "hibernate.hbm2ddl.auto";
    private static final String MAIN_PACKAGE = "com.gl.training.dibrivnyi.planner";
    private JdbcProperties jdbcProperties;
    private HibernateProperties hibernateProperties;

    @Autowired
    public void setHibernateProperties(HibernateProperties hibernateProperties) {
        this.hibernateProperties = hibernateProperties;
    }

    @Autowired
    public void setJdbcProperties(JdbcProperties jdbcProperties) {
        this.jdbcProperties = jdbcProperties;
    }

    @Bean
    public DataSource dataSource() {
        JndiTemplate jndiTemplate = new JndiTemplate();
        DataSource dataSource;
        try {
            dataSource = (DataSource) jndiTemplate.lookup(jdbcProperties.getJndiName());
        } catch (NamingException e) {
            throw new RuntimeException(e.getMessage());
        }
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(MAIN_PACKAGE);

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(jpaProperties());
        return em;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    private Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty(HIBERNATE_DIALECT, hibernateProperties.getHibernateDialect());
        properties.setProperty(HIBERNATE_SHOW_SQL, hibernateProperties.getHibernateShowSql());
        properties.setProperty(HIBERNATE_HBM_2_DLL, hibernateProperties.getHibernateHbm2Ddl());
        return properties;
    }
}
