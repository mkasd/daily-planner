package com.gl.training.dibrivnyi.planner.common.api;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:15
 */
public interface Converter<ENTITY, DTO> {

    public DTO convertToDto(ENTITY entity);
    public ENTITY convertToEntity(DTO dto);

}
