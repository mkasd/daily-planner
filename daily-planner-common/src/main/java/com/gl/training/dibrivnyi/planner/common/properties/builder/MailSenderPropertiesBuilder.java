package com.gl.training.dibrivnyi.planner.common.properties.builder;

import com.gl.training.dibrivnyi.planner.common.properties.MailSenderProperties;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:26
 */
public class MailSenderPropertiesBuilder {
    private String host;
    private String port;
    private String protocol;
    private String username;
    private String password;

    private MailSenderPropertiesBuilder() {
    }

    public static MailSenderPropertiesBuilder aMailSenderProperties() {
        return new MailSenderPropertiesBuilder();
    }

    public MailSenderPropertiesBuilder withHost(String host) {
        this.host = host;
        return this;
    }

    public MailSenderPropertiesBuilder withPort(String port) {
        this.port = port;
        return this;
    }

    public MailSenderPropertiesBuilder withProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public MailSenderPropertiesBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public MailSenderPropertiesBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public MailSenderProperties build() {
        MailSenderProperties mailSenderProperties = new MailSenderProperties();
        mailSenderProperties.setHost(host);
        mailSenderProperties.setPort(port);
        mailSenderProperties.setProtocol(protocol);
        mailSenderProperties.setUsername(username);
        mailSenderProperties.setPassword(password);
        return mailSenderProperties;
    }
}
