package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.model.Task;
import com.gl.training.dibrivnyi.planner.middle.repository.DailyPlanRepository;
import com.gl.training.dibrivnyi.planner.middle.repository.TaskRepository;
import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alexandr Dibrivnyi
 *         12.08.14
 *         16:10
 */
public class TaskServiceImplTest {

    @Mock
    private DailyPlanRepository dailyPlanRepository;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private TaskServiceImpl taskService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @After
    public void tearDown() {
        verifyZeroInteractions(dailyPlanRepository, taskRepository);
    }

    @Test
    public void createDailyPlan() throws Exception {
        User user = getUser("login", "email");
        Date dailyPlanDate = new Date();
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setUser(user);
        dailyPlan.setDayTasks(new ArrayList<Task>());
        dailyPlan.setDate(dailyPlanDate);
        dailyPlan.setState(DailyPlanState.OPENED);
        when(dailyPlanRepository.saveAndFlush(any(DailyPlan.class))).thenReturn(dailyPlan);
        when(userRepository.findOneByLogin(anyString())).thenReturn(user);

        taskService.createDailyPlan(dailyPlanDate, user.getLogin());

        verify(dailyPlanRepository).findOneByDateAndOwner(any(Date.class), anyString());
        verify(dailyPlanRepository).saveAndFlush(dailyPlan);
        verify(userRepository).findOneByLogin(user.getLogin());
    }

    @Test(expected = NullPointerException.class)
    public void createDailyPlan_null_user() throws Exception {
        taskService.createDailyPlan(new Date(), null);
    }

    @Test(expected = NullPointerException.class)
    public void createDailyPlan_null_date() throws Exception {
        taskService.createDailyPlan(null, "user");
    }

    @Test(expected = NullPointerException.class)
    public void testAddTaskToDailyPlan_null_task() throws Exception {
        taskService.addTaskToDailyPlan(null, new DailyPlan());
    }

    @Test(expected = NullPointerException.class)
    public void testAddTaskToDailyPlan_null_daily_plan() throws Exception {
        taskService.addTaskToDailyPlan(new Task(), null);
    }

    @Test
    public void testAddTaskToDailyPlan() throws Exception {
        User user = getUser("login", "email");
        Date dailyPlanDate = new Date();
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setUser(user);
        dailyPlan.setDayTasks(new ArrayList<Task>());
        dailyPlan.setDate(dailyPlanDate);

        Task task = new Task();
        task.setDailyPlan(dailyPlan);
        task.setTitle("task");

        when(taskRepository.saveAndFlush(any(Task.class))).thenReturn(task);
        when(dailyPlanRepository.saveAndFlush(any(DailyPlan.class))).thenReturn(dailyPlan);

        taskService.addTaskToDailyPlan(task, dailyPlan);

        verify(taskRepository).saveAndFlush(task);
        verify(dailyPlanRepository).saveAndFlush(dailyPlan);
    }

    @Test
    public void testDeleteDailyPlan() throws Exception {
        User user = getUser("login", "email");
        Date dailyPlanDate = new Date();
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setUser(user);
        dailyPlan.setDayTasks(new ArrayList<Task>());
        dailyPlan.setDate(dailyPlanDate);
        when(dailyPlanRepository.findOne(anyLong())).thenReturn(dailyPlan);

        taskService.deleteDailyPlan(1L);

        verify(dailyPlanRepository).findOne(1L);
        verify(dailyPlanRepository).delete(dailyPlan);
    }

    @Test
    public void testDeleteDailyPlan_null_id() throws Exception {
        when(dailyPlanRepository.findOne(null)).thenReturn(null);

        taskService.deleteDailyPlan(null);

        verify(dailyPlanRepository).findOne(null);
    }

    @Test
    public void testDeleteTask() throws Exception {
        Task task = new Task();
        task.setTitle("title");
        when(taskRepository.findOne(anyLong())).thenReturn(task);

        taskService.deleteTask(1L);

        verify(taskRepository).findOne(1L);
        verify(taskRepository).delete(task);
    }

    @Test
    public void testDeleteTask_null_id() throws Exception {
        when(taskRepository.findOne(null)).thenReturn(null);

        taskService.deleteTask(null);

        verify(taskRepository).findOne(null);
    }

    @Test
    public void testListAllDailyPlans_empty_list() throws Exception {
        List<DailyPlan> dailyPlanList = mock(List.class);
        when(dailyPlanRepository.findAll()).thenReturn(dailyPlanList);

        List<DailyPlan> allDailyPlans = taskService.findAllDailyPlans();

        assertThat(dailyPlanList).isEqualTo(allDailyPlans);
        assertThat(dailyPlanList).hasSize(0);

        verify(dailyPlanRepository).findAll();
        verify(dailyPlanList, times(2)).size();
        verifyNoMoreInteractions(dailyPlanList);
    }

    @Test
    public void testListAllDailyPlans() throws Exception {
        List<DailyPlan> dailyPlanList = Arrays.asList(new DailyPlan());
        when(dailyPlanRepository.findAll()).thenReturn(dailyPlanList);

        List<DailyPlan> allDailyPlans = taskService.findAllDailyPlans();

        assertThat(dailyPlanList).isEqualTo(allDailyPlans);
        assertThat(dailyPlanList).hasSize(1);

        verify(dailyPlanRepository).findAll();
    }

    @Test
    public void findAllDailyPlansForUser_empty_list() throws Exception {
        List<DailyPlan> dailyPlanList = mock(List.class);
        when(dailyPlanRepository.findAllDailyPlansForUser(anyString())).thenReturn(dailyPlanList);

        List<DailyPlan> allDailyPlans = taskService.findAllDailyPlansForUser("login");

        assertThat(dailyPlanList).isEqualTo(allDailyPlans);
        assertThat(dailyPlanList).hasSize(0);

        verify(dailyPlanRepository).findAllDailyPlansForUser("login");
        verify(dailyPlanList, times(2)).size();
        verifyNoMoreInteractions(dailyPlanList);
    }

    @Test
    public void findAllDailyPlansForUser() throws Exception {
        List<DailyPlan> dailyPlanList = Arrays.asList(new DailyPlan());
        when(dailyPlanRepository.findAllDailyPlansForUser(anyString())).thenReturn(dailyPlanList);

        List<DailyPlan> allDailyPlans = taskService.findAllDailyPlansForUser("login");

        assertThat(dailyPlanList).isEqualTo(allDailyPlans);
        assertThat(dailyPlanList).hasSize(1);

        verify(dailyPlanRepository).findAllDailyPlansForUser("login");
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteTasks_invalid_tasks() throws Exception {
        taskService.deleteTasks(null);
    }

    @Test
    public void testDeleteTasks() throws Exception {
        taskService.deleteTasks(new ArrayList<Task>());

        verify(taskRepository).delete(any(List.class));
    }

    private User getUser(String login, String email) {
        User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        return user;
    }
}
