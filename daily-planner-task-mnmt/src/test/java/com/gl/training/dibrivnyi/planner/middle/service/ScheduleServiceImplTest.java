package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.repository.DailyPlanRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:57
 */
public class ScheduleServiceImplTest {

    @Mock
    private DailyPlanRepository dailyPlanRepository;
    @InjectMocks
    private ScheduleServiceImpl scheduleService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dailyPlanRepository);
    }

    @Test
    public void testCloseDailyPlans_empty() throws Exception {
        when(dailyPlanRepository.findAllOpenedDailyPlansBeforeDate(any(Date.class))).thenReturn(new ArrayList<DailyPlan>());

        scheduleService.closeDailyPlans();

        verify(dailyPlanRepository).findAllOpenedDailyPlansBeforeDate(any(Date.class));
    }

    @Test
    public void testCloseDailyPlans() throws Exception {
        List<DailyPlan> dailyPlans = new ArrayList<>();
        dailyPlans.add(getDailyPlan());
        dailyPlans.add(getDailyPlan());

        when(dailyPlanRepository.findAllOpenedDailyPlansBeforeDate(any(Date.class))).thenReturn(dailyPlans);
        when(dailyPlanRepository.save(any(List.class))).thenReturn(dailyPlans);

        scheduleService.closeDailyPlans();

        assertThat(dailyPlans).extracting("state").containsOnly(DailyPlanState.CLOSED);
        verify(dailyPlanRepository).findAllOpenedDailyPlansBeforeDate(any(Date.class));
        verify(dailyPlanRepository).save(dailyPlans);
    }

    private DailyPlan getDailyPlan() {
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setState(DailyPlanState.OPENED);
        dailyPlan.setDate(new Date());
        return dailyPlan;
    }
}
