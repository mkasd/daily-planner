package com.gl.training.dibrivnyi.planner.middle.util;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Alexandr Dibrivnyi
 *         16.08.14
 *         22:33
 */
public class DateUtilTest {

    private DateUtil dateUtil;

    @Before
    public void setUp() {
        dateUtil = new DateUtil();
    }

    @Test
    public void testIsDayBefore_future_date() throws Exception {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2014, Calendar.DECEMBER, 31);
        Date selectedDate = calendar.getTime();

        assertThat(dateUtil.isDayBefore(selectedDate)).isFalse();
    }

    @Test
    public void testIsDayBefore_past_date() throws Exception {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2014, Calendar.JANUARY, 31);
        Date selectedDate = calendar.getTime();

        assertThat(dateUtil.isDayBefore(selectedDate));
    }

    @Test
    public void testIsDayBefore_current_date() {
        Date currentDate = new Date();

        assertThat(dateUtil.isDayBefore(currentDate)).isFalse();
    }
}
