package com.gl.training.dibrivnyi.planner.middle.repository;

import com.gl.training.dibrivnyi.planner.middle.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         23:00
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}