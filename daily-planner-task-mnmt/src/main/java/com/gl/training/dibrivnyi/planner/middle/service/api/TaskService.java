package com.gl.training.dibrivnyi.planner.middle.service.api;

import com.gl.training.dibrivnyi.planner.middle.exception.DuplicatedPlanException;
import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.model.Task;
import com.gl.training.dibrivnyi.planner.user.model.User;

import java.util.Date;
import java.util.List;

/**
 * @author oleksandr.dibrivniy
 *         8/7/14
 *         3:13 PM
 */
public interface TaskService {

    void createDailyPlan(Date date, String ownerLogin) throws DuplicatedPlanException;

    void addTaskToDailyPlan(Task task, DailyPlan dailyPlan);

    void deleteDailyPlan(Long id);

    void deleteTask(Long id);

    void deleteTasks(List<Task> tasks);

    List<DailyPlan> findAllDailyPlans();

    List<DailyPlan> findAllDailyPlansForUser(String login);
}
