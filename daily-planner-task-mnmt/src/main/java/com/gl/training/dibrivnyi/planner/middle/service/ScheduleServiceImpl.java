package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.repository.DailyPlanRepository;
import com.gl.training.dibrivnyi.planner.middle.service.api.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:36
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger LOGGER = Logger.getLogger(ScheduleServiceImpl.class.getSimpleName());
    private DailyPlanRepository dailyPlanRepository;

    @Autowired
    public void setDailyPlanRepository(DailyPlanRepository dailyPlanRepository) {
        this.dailyPlanRepository = dailyPlanRepository;
    }

    /**
     * Method for scheduling closing task. Runs every date at 0.10 AM
     */
    @Scheduled(cron = "0 10 0 * * ?")
    public void closeDailyPlans() {
        Date currentDate = new Date();
        List<DailyPlan> openedDailyPlans = dailyPlanRepository.findAllOpenedDailyPlansBeforeDate(currentDate);
        if (!openedDailyPlans.isEmpty()) {
            for (DailyPlan openedDailyPlan : openedDailyPlans) {
                openedDailyPlan.setState(DailyPlanState.CLOSED);
            }
            dailyPlanRepository.save(openedDailyPlans);
            LOGGER.info(String.format("Closed <%s> daily plans", openedDailyPlans.size()));
        } else LOGGER.info("There are no opened daily plans");
    }
}
