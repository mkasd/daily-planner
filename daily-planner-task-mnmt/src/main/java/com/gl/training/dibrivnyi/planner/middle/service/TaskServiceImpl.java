package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.exception.DuplicatedPlanException;
import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.model.Task;
import com.gl.training.dibrivnyi.planner.middle.repository.DailyPlanRepository;
import com.gl.training.dibrivnyi.planner.middle.repository.TaskRepository;
import com.gl.training.dibrivnyi.planner.middle.service.api.TaskService;
import com.gl.training.dibrivnyi.planner.user.model.User;
import com.gl.training.dibrivnyi.planner.user.repository.UserRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author oleksandr.dibrivniy
 *         8/7/14
 *         3:21 PM
 */
@Service
public class TaskServiceImpl implements TaskService {

    private static final Logger LOGGER = Logger.getLogger(TaskServiceImpl.class.getSimpleName());
    private DailyPlanRepository dailyPlanRepository;
    private TaskRepository taskRepository;
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setDailyPlanRepository(DailyPlanRepository dailyPlanRepository) {
        this.dailyPlanRepository = dailyPlanRepository;
    }

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Method for creating new daily plan.
     *
     * @param date       - date plan
     * @param ownerLogin - plan owner
     * @throws DuplicatedPlanException - throws if pla already exists
     */
    @Override
    public void createDailyPlan(Date date, String ownerLogin) throws DuplicatedPlanException {
        Validate.notNull(date);
        Validate.notEmpty(ownerLogin);

        DailyPlan plan = dailyPlanRepository.findOneByDateAndOwner(date, ownerLogin);
        if (plan != null) {
            String errorMessage = String.format("Duplicated plan for user <%s> and date <%s>", ownerLogin, date);
            LOGGER.warning(errorMessage);
            throw new DuplicatedPlanException(errorMessage);
        }

        User owner = userRepository.findOneByLogin(ownerLogin);
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setDate(date);
        dailyPlan.setDayTasks(new ArrayList<Task>());
        dailyPlan.setUser(owner);
        dailyPlan.setState(DailyPlanState.OPENED);

        dailyPlanRepository.saveAndFlush(dailyPlan);
        LOGGER.info(String.format("Daily plan %s was added to database", dailyPlan));
    }

    /**
     * Method for adding new task to selected daily plan
     *
     * @param task      - new task
     * @param dailyPlan - selected plan
     */
    @Override
    public void addTaskToDailyPlan(Task task, DailyPlan dailyPlan) {
        Validate.notNull(task);
        Validate.notNull(dailyPlan);
        dailyPlan.getDayTasks().add(task);

        taskRepository.saveAndFlush(task);
        dailyPlanRepository.saveAndFlush(dailyPlan);
        LOGGER.info(String.format("New task %s was added to database", task));
        LOGGER.info(String.format("Daily plan %s was updated", dailyPlan.getId()));
    }

    /**
     * Method for deleting plan by its id
     *
     * @param id - plan id
     */
    @Override
    public void deleteDailyPlan(Long id) {
        DailyPlan dailyPlan = dailyPlanRepository.findOne(id);
        if (dailyPlan != null) {
            dailyPlanRepository.delete(dailyPlan);
            LOGGER.info(String.format("Daily plan %s was removed", id));
        }
    }

    /**
     * Method for task plan by its id
     *
     * @param id - task id
     */
    @Override
    public void deleteTask(Long id) {
        Task task = taskRepository.findOne(id);
        if (task != null) {
            taskRepository.delete(task);
            LOGGER.info(String.format("Task %s was removed", id));
        }
    }

    /**
     * Method for deleting list of tasks
     * @param tasks - tasks to remove
     */
    @Override
    public void deleteTasks(List<Task> tasks) {
        Validate.notNull(tasks);
        taskRepository.delete(tasks);
        LOGGER.info(String.format("Deleted <%s> tasks", tasks.size()));
    }

    /**
     * Method for loading all daily plans
     * @return - list of all daily plans
     */
    @Override
    public List<DailyPlan> findAllDailyPlans() {
        List<DailyPlan> dailyPlans = dailyPlanRepository.findAll();
        LOGGER.info("Totally loaded daily plans: " + dailyPlans.size());
        return dailyPlans;
    }

    /**
     * Method for loading all daily plans for selected users
     * @param login - selected user login
     * @return - all daily plans for selected users
     */
    @Override
    public List<DailyPlan> findAllDailyPlansForUser(String login) {
        List<DailyPlan> dailyPlans = dailyPlanRepository.findAllDailyPlansForUser(login);
        LOGGER.info("Totally loaded daily plans: " + dailyPlans.size());
        return dailyPlans;
    }
}
