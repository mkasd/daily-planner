package com.gl.training.dibrivnyi.planner.middle.repository;

import com.gl.training.dibrivnyi.planner.middle.model.DailyPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         23:01
 */
@Repository
public interface DailyPlanRepository extends JpaRepository<DailyPlan, Long> {

    /**
     * Method for loading all daily plan for selected user
     *
     * @param login - user login
     * @return - list of daily plans for selected users
     */
    @Query("select dp from DailyPlan dp where dp.user.login =:login order by dp.date asc")
    public List<DailyPlan> findAllDailyPlansForUser(@Param("login") String login);

    /**
     * Method for loading all opened daily plans before selected date
     *
     * @param currentDate - selected date
     * @return - list of all opened daily plans before selected date
     */
    @Query("select dp from DailyPlan dp where dp.state = 'OPENED' and dp.date <= :currentDate")
    public List<DailyPlan> findAllOpenedDailyPlansBeforeDate(@Param("currentDate") Date currentDate);

    /**
     * Method for loading daily plan by owner and plan date
     *
     * @param planDate   - selected date
     * @param ownerLogin - plan owner
     * @return - plan details
     */
    @Query("select dp from DailyPlan dp where dp.date =:date and dp.user.login =:login")
    public DailyPlan findOneByDateAndOwner(@Param("date") Date planDate, @Param("login") String ownerLogin);
}
