package com.gl.training.dibrivnyi.planner.middle;

import com.gl.training.dibrivnyi.planner.common.PersistenceConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         22:38
 */
@Configuration
@ComponentScan(basePackageClasses = DailyPlannerManagementConfiguration.class)
@EnableJpaRepositories(basePackageClasses = DailyPlannerManagementConfiguration.class)
@Import(PersistenceConfiguration.class)
@EnableScheduling
public class DailyPlannerManagementConfiguration {
}
