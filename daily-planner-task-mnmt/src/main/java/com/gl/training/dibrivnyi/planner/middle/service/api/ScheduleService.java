package com.gl.training.dibrivnyi.planner.middle.service.api;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:56
 */
public interface ScheduleService {

    void closeDailyPlans();
}
